import Vue from "vue";
import VueLodash from "vue-lodash";
import App from "./App.vue";
import router from "./router";
import AlertPlugin from "./plugins/alert-plugin";
// import Loading from "./components/Loading.vue";
import loadingDirective from "./directives/loading";

Vue.config.productionTip = false;

Vue.use(VueLodash, {
  name: "lodash"
});

Vue.use(loadingDirective);

Vue.directive("highlight", {
  bind(el, binding, vnode) {
    // console.log("bind");
    // console.log("el", el);
    var delay = 0;
    if (binding.modifiers["delayed"]) {
      delay = 2000;
    }
    setTimeout(() => {
      if (binding.arg === "background") {
        el.style.backgroundColor = binding.value;
      } else {
        el.style.color = binding.value;
      }
    }, delay);
  }

  // inserted(el, binding, vnode) {
  //   // .el-loading-parent--relative
  //   console.log("==========>INSERTED");
  //   console.log("vnode", vnode);
  // },

  // update(el, binding, vnode, oldVnode) {
  //   console.log("==========>UPDATE");
  //   console.log("vnode", vnode);
  //   console.log("oldVnode", oldVnode);
  // },

  // componentUpdated(el, binding, vnode, oldVnode) {
  //   console.log("==========>COMPONENTUPDATED");
  //   console.log("vnode", vnode);
  //   console.log("oldVnode", oldVnode);
  // }
});

// Vue.directive("loading", {
//   bind(el, binding, vnode) {
//     // const mask = el.querySelector(".el-loading-mask");
//     // if (binding.value) {
//     //   mask.style.display = "block";
//     // }
//     console.log(el);
//     if (!el.domVisible) {
//     }
//   }
// });

Vue.use(AlertPlugin);

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
