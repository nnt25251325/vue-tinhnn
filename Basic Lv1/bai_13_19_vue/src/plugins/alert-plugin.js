export default {
    install(Vue, options) {
        Vue.mixin({
            methods: {
                $_notice(message) {
                    window.alert(message);
                }
            }
        })
    }
};
