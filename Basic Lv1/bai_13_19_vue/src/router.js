import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Bai13Vue from "./views/Bai13Vue.vue";
import Bai14Vue from "./views/Bai14Vue.vue";
import Bai15Vue from "./views/Bai15Vue.vue";
import Bai17Vue from "./views/Bai17Vue.vue";
import Bai18Vue from "./views/Bai18Vue.vue";
import Bai19Vue from "./views/Bai19Vue.vue";
import Computed from "./views/Computed.vue";
import Watch from "./views/Watch.vue";
import Directives from "./views/Directives.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    // {
    //   path: "/about",
    //   name: "about",
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: About.vue
    // },
    {
      path: "/bai13vue",
      name: "bai13vue",
      component: Bai13Vue
    },
    {
      path: "/bai14vue",
      name: "bai14vue",
      component: Bai14Vue
    },
    {
      path: "/bai15vue",
      name: "bai15vue",
      component: Bai15Vue
    },
    {
      path: "/bai17vue",
      name: "bai17vue",
      component: Bai17Vue
    },
    {
      path: "/bai18vue",
      name: "bai18vue",
      component: Bai18Vue
    },
    {
      path: "/bai19vue",
      name: "bai19vue",
      component: Bai19Vue
    },
    {
      path: "/computed",
      name: "computed",
      component: Computed
    },
    {
      path: "/watch",
      name: "watch",
      component: Watch
    },
    {
      path: "/directives",
      name: "directives",
      component: Directives
    }
  ]
});
