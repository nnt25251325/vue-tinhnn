const CATEGORIES = [
  { key: 0, value: 'Thời sự' },
  { key: 1, value: 'Công nghệ' },
  { key: 2, value: 'Thế giới' },
  { key: 3, value: 'Kinh doanh' },
  { key: 4, value: 'Giải trí' },
  { key: 5, value: 'Thể thao' }
]

const POSITIONS = [
  { key: 1, value: 'Việt Nam' },
  { key: 2, value: 'Châu Á' },
  { key: 3, value: 'Châu Âu' },
  { key: 4, value: 'Châu Mỹ' },
]

export default {
  CATEGORIES,
  POSITIONS
}
