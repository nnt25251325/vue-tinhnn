import axios from 'axios';

const API_URL = 'http://localhost:4000';

export default function callApi(endpoint, method = 'GET', body) {
  return axios({
    method: method,
    url: `${API_URL}/${endpoint}`,
    data: body
  });
};
